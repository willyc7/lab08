package it.unibo.oop.lab.mvcio;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 
 */
public class Controller {

    private static final String HOME = System.getProperty("user.home");
    private static final String SEPARATOR = System.getProperty("file.separator");
    /*
     * This class must implement a simple controller responsible of I/O access. It
     * considers a single file at a time, and it is able to serialize objects in it.
     * 
     * Implement this class with:
     * 
     * 1) A method for setting a File as current file
     * 
     * 2) A method for getting the current File
     * 
     * 3) A method for getting the path (in form of String) of the current File
     * 
     * 4) A method that gets a String as input and saves its content on the current
     * file. This method may throw an IOException.
     * 
     * 5) By default, the current file is "output.txt" inside the user home folder.
     * A String representing the local user home folder can be accessed using
     * System.getProperty("user.home"). The separator symbol (/ on *nix, \ on
     * Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods leads
     * to a software that runs correctly on every platform.
     */
    private File actualFile;

    /**
     * Constructor of a Controller.
     */
    public Controller() {
        this.actualFile = new File(HOME + SEPARATOR + "output.txt");
    }

    /**
     * @param file   the new file
     * 
     * @return 1 if something went wrong
     */
    public int setCurrFile(final File file) {
        if (file.exists()) {
            this.actualFile = file;
            return 0;
        }
        return 1;
    }

    /**
     * Method that creates a copy of the actual file and returns it.
     * @return File the copy of the actual file
     * @throws IOException when creating the copy
     */
    public File getFile() throws IOException {
        return new File(this.actualFile.getCanonicalPath());
    }

    /**
     * 
     * @return the path of the actual file
     */
    public String getFileName() {
        return this.actualFile.getPath();
    }

    /**
     * 
     * @param content the string to add
     * @throws IOException if its impossible to write
     */
    public void addContent(final String content) throws IOException {
        FileWriter os = new FileWriter(this.actualFile);
        BufferedWriter writer = new BufferedWriter(os);
        writer.append(content);
        writer.close();
        os.close();
    }
}
