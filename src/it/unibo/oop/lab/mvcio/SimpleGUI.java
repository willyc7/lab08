package it.unibo.oop.lab.mvcio;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUI {

    private static final int TEXTDIMCOEF = 15;
    private final JFrame frame = new JFrame("MVC first app");
    private final Controller ctrl = new Controller();
    private static final String FOCUSTEXT = "Insert file name here";
    private static final String ERRORTEXT = "Error while trying to insert file";

    /*
     * Once the Controller is done, implement this class in such a way that:
     * 
     * 1) I has a main method that starts the graphical application
     * 
     * 2) In its constructor, sets up the whole view
     * 
     * 3) The graphical interface consists of a JTextArea with a button "Save" right
     * below (see "ex02.png" for the expected result). SUGGESTION: Use a JPanel with
     * BorderLayout
     * 
     * 4) By default, if the graphical interface is closed the program must exit
     * (call setDefaultCloseOperation)
     * 
     * 5) The program asks the controller to save the file if the button "Save" gets
     * pressed.
     * 
     * Use "ex02.png" (in the res directory) to verify the expected aspect.
     */

    /**
     * builds a new {@link SimpleGUI}.
     */
    public SimpleGUI() {
        /*
         * Make the frame half the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the
         * primary is selected.
         * 
         * In order to deal coherently with multimonitor setups, other
         * facilities exist (see the Java documentation about this issue). It is
         * MUCH better than manually specify the size of a window in pixel: it
         * takes into account the current resolution.
         */
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //adding the main panel to the frame
        JPanel mainPanel = new JPanel(new BorderLayout());
        frame.getContentPane().add(mainPanel);
        //creating components for the main panel
        JTextArea txtFileName = new JTextArea(FOCUSTEXT);
        txtFileName.setFont(new Font(Font.SANS_SERIF, Frame.NORMAL, (int) frame.getHeight() / TEXTDIMCOEF));
        JButton btnSave = new JButton("Save");
        //adding the components to the panel
        mainPanel.add(txtFileName, BorderLayout.CENTER);
        mainPanel.add(btnSave, BorderLayout.SOUTH);
        //Setting action listeners
        btnSave.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                int res;
                if (!txtFileName.getText().equals("")) {
                    File nFile = new File(txtFileName.getText());
                    res = SimpleGUI.this.ctrl.setCurrFile(nFile);
                    if (res == 1) {
                        txtFileName.setText(ERRORTEXT);
                    } else {
                        txtFileName.setText("Done");
                    }
                }
            }
        });
    }

    /**
     * Shows the GUI.
     */
    public void display() {
        this.frame.setVisible(true);
    }

    /**
     * Main.
     * @param args params of main.
     */
    public static void main(final String... args) {
        SimpleGUI gui = new SimpleGUI();
        gui.display();
    }

}
