package it.unibo.oop.lab.mvcio2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import it.unibo.oop.lab.mvcio.Controller;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser {

    private static final int TEXTDIMCOEF = 15;
    private final JFrame frame = new JFrame("MVC second");
    private final Controller ctrl = new Controller();
    private static final String FOCUSTEXT = "Insert file name here"; 
    private static final String ERRORTEXT = "Error while trying to insert file";
    /*
     * TODO: Starting from the application in mvcio:
     * 
     * 1) Add a JTextField and a button "Browse..." on the upper part of the
     * graphical interface.
     * Suggestion: use a second JPanel with a second BorderLayout, put the panel
     * in the North of the main panel, put the text field in the center of the
     * new panel and put the button in the line_end of the new panel.
     * 
     * 2) The JTextField should be non modifiable. And, should display the
     * current selected file.
     * 
     * 3) On press, the button should open a JFileChooser. The program should
     * use the method showSaveDialog() to display the file chooser, and if the
     * result is equal to JFileChooser.APPROVE_OPTION the program should set as
     * new file in the Controller the file chosen. If CANCEL_OPTION is returned,
     * then the program should do nothing. Otherwise, a message dialog should be
     * shown telling the user that an error has occurred (use
     * JOptionPane.showMessageDialog()).
     * 
     * 4) When in the controller a new File is set, also the graphical interface
     * must reflect such change. Suggestion: do not force the controller to
     * update the UI: in this example the UI knows when should be updated, so
     * try to keep things separated.
     */
    /**
     * builds a new {@link SimpleGUI}.
     */
    public SimpleGUIWithFileChooser() {
        /*
         * Make the frame half the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the
         * primary is selected.
         * 
         * In order to deal coherently with multimonitor setups, other
         * facilities exist (see the Java documentation about this issue). It is
         * MUCH better than manually specify the size of a window in pixel: it
         * takes into account the current resolution.
         */
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //adding the main panel to the frame
        JPanel mainPanel = new JPanel(new BorderLayout());
        frame.getContentPane().add(mainPanel);
        //creating components for the main panel
        JTextArea txtFileName = new JTextArea(FOCUSTEXT);
        txtFileName.setFont(new Font(Font.SANS_SERIF, Frame.NORMAL, (int) frame.getHeight() / TEXTDIMCOEF));
        JButton btnSave = new JButton("Save");
        JPanel pnlSecond = new JPanel(new BorderLayout());
        JButton btnBrowse = new JButton("Browse");
        JTextField fldActFile = new JTextField();
        fldActFile.setText(this.ctrl.getFileName());
        fldActFile.setEditable(false);
        //adding the components to the panel
        pnlSecond.add(fldActFile, BorderLayout.CENTER);
        pnlSecond.add(btnBrowse, BorderLayout.LINE_END);
        mainPanel.add(pnlSecond, BorderLayout.NORTH);
        mainPanel.add(txtFileName, BorderLayout.CENTER);
        mainPanel.add(btnSave, BorderLayout.SOUTH);
        //Setting action listeners
        btnSave.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                int res;
                if (!txtFileName.getText().equals("")) {
                    File nFile = new File(txtFileName.getText());
                    res = ctrl.setCurrFile(nFile);
                    if (res == 1) {
                        txtFileName.setText(ERRORTEXT);
                    } else {
                        txtFileName.setText("Done");
                        fldActFile.setText(ctrl.getFileName());
                    }
                }
            }
        });

        btnBrowse.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                JFileChooser fc = new JFileChooser();
                final int res = fc.showSaveDialog(frame);
                if (res == JFileChooser.APPROVE_OPTION) {
                    File sel = fc.getSelectedFile();
                    ctrl.setCurrFile(sel);
                    fldActFile.setText(ctrl.getFileName());
                    JOptionPane.showMessageDialog(frame, "It worked");
                } else if (!(res == JFileChooser.CANCEL_OPTION)) {
                    JOptionPane.showMessageDialog(frame, "An error occurred");
                }
            }
        });
    }

    /**
     * Shows the GUI.
     */
    public void display() {
        this.frame.setVisible(true);
    }

    /**
     * Main.
     * @param args params of main.
     */
    public static void main(final String... args) {
        SimpleGUIWithFileChooser gui = new SimpleGUIWithFileChooser();
        gui.display();
    }

}
