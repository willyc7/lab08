package it.unibo.oop.lab.advanced;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 
 *
 */
public class DrawNumberViewLog implements DrawNumberView{
    private BufferedWriter logFile;

    /**
     * 
     */
    public DrawNumberViewLog() {
    }

    @Override
    public void setObserver(final DrawNumberViewObserver observer) {
    }

    @Override
    public void start() {
        String separator = System.getProperty("file.separator");
        File file = new File(System.getProperty("user.home") + separator + "logNumber.txt");
        if (!file.exists()) {
            try {
                if (!file.createNewFile()) {
                    System.out.println("Error creating the log\n");
                }
                file.setWritable(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            this.logFile = new BufferedWriter(new FileWriter(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void numberIncorrect() {
        try {
            this.logFile.append("Number inserted is incorrect!\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void result(final DrawResult res) {
        try {
            this.logFile.append("Number inserted: " + res.getDescription() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void limitsReached() {
        try {
            this.logFile.append("Limits reached \n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void diplayError(final String message) {
        try {
            this.logFile.append("Error: " + message + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void close() {
        try {
            this.logFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
