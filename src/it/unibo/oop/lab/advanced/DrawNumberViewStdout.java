package it.unibo.oop.lab.advanced;

/**
 * 
 *
 */
public class DrawNumberViewStdout implements DrawNumberView {
    /**
     * 
     */
    public DrawNumberViewStdout() {
    }

    @Override
    public void setObserver(final DrawNumberViewObserver observer) {
    }

    @Override
    public void start() {
        System.out.println("Application started");
    }

    @Override
    public void numberIncorrect() {
        System.out.println("Number inserted is incorrect!");
    }

    @Override
    public void result(final DrawResult res) {
        System.out.println("Number inserted: " + res.getDescription());
    }

    @Override
    public void limitsReached() {
        System.out.println("Limits reached");
    }

    @Override
    public void diplayError(final String message) {
        System.out.println("Error: " + message);
    }


    @Override
    public void close() {
        System.out.println("Application closed");
    }
}
