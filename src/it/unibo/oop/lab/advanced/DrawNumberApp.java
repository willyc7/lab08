package it.unibo.oop.lab.advanced;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringTokenizer;

/**
 */
public final class DrawNumberApp implements DrawNumberViewObserver {

    private final DrawNumber model;
    private final List<DrawNumberView> views;
    private final Configuration configuration;

    /**
     * 
     */
    public DrawNumberApp() {
        this.configuration = this.readConfiguration();
        this.model = new DrawNumberImpl(configuration.minimum, configuration.maximum, configuration.attempts);
        this.views = new ArrayList<>();
        this.views.add(new DrawNumberViewImpl());
        this.views.add(new DrawNumberViewLog());
        this.views.add(new DrawNumberViewStdout());
        for (DrawNumberView view: this.views) {
            view.setObserver(this);
            view.start();
        }
    }

    @Override
    public void newAttempt(final int n) {
        try {
            final DrawResult result = model.attempt(n);
            for (DrawNumberView view: this.views) {
                view.result(result);
            }
        } catch (IllegalArgumentException e) {
            for (DrawNumberView view: this.views) {
                view.numberIncorrect();
            }
        } catch (AttemptsLimitReachedException e) {
            for (DrawNumberView view: this.views) {
                view.limitsReached();
            }
        }
    }

    @Override
    public void resetGame() {
        this.model.reset();
    }

    @Override
    public void quit() {
        for (DrawNumberView view: this.views) {
            view.close();
        }
        System.exit(0);
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        new DrawNumberApp();
    }

    private Configuration readConfiguration() {
        String line;
        String attr;
        int value;
        StringTokenizer strT;
        Configuration configuration = new Configuration();
        try (BufferedReader conf = new BufferedReader(
                new InputStreamReader(
                ClassLoader.getSystemResourceAsStream("config.yml")))) {
            while ((line = conf.readLine()) != null) {
                strT = new StringTokenizer(line, ":");
                if (strT.countTokens() == 2) {
                    attr = strT.nextToken().trim();
                    value = Integer.parseInt(strT.nextToken().trim());
                    if (attr.equals("minimum")) {
                        configuration.setMinimum(value);
                    } else if (attr.equals("maximum")) {
                        configuration.setMaximum(value);
                    } else if (attr.equals("attempts")) {
                        configuration.setAttempts(value);
                    }
                }
            }
        } catch (IOException e) {
        } catch (NullPointerException e2) { 
        }
        return configuration;
    }

    private static class Configuration {
        private static final int MIN = 0;
        private static final int MAX = 100;
        private static final int ATTEMPTS = 10;
        private int minimum;
        private int maximum;
        private int attempts;

        /**
         * 
         */
        Configuration() {
            this.minimum = MIN;
            this.maximum = MAX;
            this.attempts = ATTEMPTS;
        }

        private void setMinimum(final int minimum) {
            this.minimum = Objects.requireNonNull(minimum);
        }

        private void setMaximum(final int maximum) {
            this.maximum = Objects.requireNonNull(maximum);
        }

        private void setAttempts(final int attempts) {
            this.attempts = Objects.requireNonNull(attempts);
        }
    }

}
