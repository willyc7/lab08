package it.unibo.oop.lab.mvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class that models an IO controller.
 * 
 *
 */
public class MyController implements Controller {

    private String nextString;
    private List<String> history;

    /**
     * Creates a new MyController object.
     */
    public MyController() {
        this.nextString = null;
        this.history = new ArrayList<>();
    }

    /**
     * {@inheritDoc}
     */
    public void setNextString(final String next) throws NullPointerException {
        Objects.requireNonNull(next, "Invalid string");
        this.nextString = next;
    }

    /**
     * {@inheritDoc}
     */
    public String getNextString() {
        return this.nextString;
    }

    /**
     * {@inheritDoc}
     */
    public List<String> getHistory() {
        return new ArrayList<>(this.history);
    }

    /**
     * {@inheritDoc}
     */
    public void printCurrentString() throws IllegalStateException {
        if (this.nextString == null) {
            throw new IllegalStateException();
        } else {
            System.out.println(this.nextString);
            this.history.add(this.nextString);
            this.nextString = null;
        }
    }
}
